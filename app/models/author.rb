class Author < ApplicationRecord

  has_many :publications
  validates_presence_of :name, presence: true
  validates_presence_of :email,:name,:created_by
end
