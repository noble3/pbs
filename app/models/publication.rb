class Publication < ApplicationRecord

  belongs_to :author, class_name: "Author"
  validates_presence_of :title, :body
end
