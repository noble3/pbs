import React from 'react'
import ReactDOM from 'react-dom'
import App from './authors/components/App'
import {BrowserRouter as Router, Route,Switch} from "react-router-dom";
import Author from "./authors/components/Author";
import Publication from "./authors/components/Publication";

const authors = document.querySelector('#authors');

const Routes = () => (

    <Router>
        <div>
            <Switch>
                <Route exact path="/" component={App} />
                <Route exact path="/authors/" component={Author} />
                <Route  path="/authors/:id/publications" component={Publication}/>
            </Switch>
        </div>
    </Router>
)

ReactDOM.render(<Routes/>, authors);