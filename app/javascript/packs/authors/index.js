import React  from  'react'
import ReactDOM from 'react-dom'
import App from "./components/App";

const authors = document.querySelector('#authors')

ReactDOM.render(<App/>, authors);

