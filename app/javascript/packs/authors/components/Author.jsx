import React from  'react'
import Cards from './Card'
import axios from "axios";

class  Author extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            authors: [],
            loading: false,
        }
    }

    fetchAuthors(){
        this.setState({loading: true});
        axios.get('api/authors')
            .then(response => {
                this.setState({
                    authors: response.data.map((author) =>  {return author }),
                    loading: false
                })
            })
            .catch(err => {
                console.error(err)
            })
    }


    componentDidMount() {
        this.fetchAuthors()
    }

    componentWillReceiveProps() {
        this.fetchAuthors()
    }

    render() {
        return(
            <div>
                <Cards isLoading={this.state.loading} author={this.state.authors} />
            </div>
        )
    }
}

export  default Author;