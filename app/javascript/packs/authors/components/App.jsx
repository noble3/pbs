import  React from 'react'
import axios from 'axios'
import Author from "./Author";
import Publication from "./Publication";

class App extends React.Component{

    constructor () {
        super()
        this.state = {
            publications: false,
            loading: false
        }
    }


    render() {

        let styles = {
            authorStyles: {
                width: '35%',
                margin: '0 auto'
            }
        };

        return(
            <div>
                <div style={styles.authorStyles}>
                    <header>
                        <h2>Authors</h2>
                    </header>
                </div>
                { this.state.publications === false ? this.state.loading === false ? (
                        <Author />
                    ): (<Publication />):(false)
                }
            </div>
        )
    }
}


export default App;