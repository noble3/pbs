import React from 'react'
import Card from '../components/Card'
import axios from "axios";
class Publication extends React.Component{

    constructor (props) {
        super(props);
        this.state = {
            publications: [],
            isLoading: false
        }
    }


    fetchPublications () {
        this.setState({ isLoading: true });
        axios.get(`http://localhost:3000/api/authors/1/publications/`)
            .then(response =>
                this.setState({
                    publications: response.data.map((publication) => {return publication}),
                    isLoading: false
                })
            )
            .catch(err => {
                console.error(err)
            })
    }

    componentDidMount() {
        this.fetchPublications();
    }

    componentWillReceiveProps() {
        this.fetchPublications();
    }

    render() {

        const { isLoading } = this.state;
        if (isLoading) {
            return <p>Loading ...</p>;
        }

        return(
            <div>
                <Card isLoading={this.state.isLoading} publication={this.state.publications}/>
            </div>
        )
    }
}

export default Publication;
