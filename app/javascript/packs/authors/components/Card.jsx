import React from 'react'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import Button  from '@material-ui/core/Button'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import { withStyles } from '@material-ui/core/styles';
import { Link } from "react-router-dom";
import Fab from "@material-ui/core/es/Fab/Fab";
import AddIcon from '@material-ui/icons/Add';

class Cards extends React.Component{

    constructor(props) {
        super(props);
            this.state = {
                changed: false,
            }
    }

    render(){

            let { author } = this.props;
            let { publication } = this.props;
            let { classes } = this.props;


            let {isLoading} = this.props;

            if(isLoading) {
                return <p>Loading ...</p>;
            }

        return(
             <div>
                {this.props.author !== undefined ? (author.map((author =>
                    (<Card className={classes.card} key={author.id}>
                    <CardContent>
                        <Typography className={classes.title} variant="h5" component="h2" color={"textSecondary"}  gutterBottom>
                            { author.name}
                        </Typography>
                        <Typography className={classes.email}  component="p"  color={"textSecondary"}>
                            { author.email }
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Link to={`/authors/${author.id}/publications`} className={classes.a}><Button size="small" className={classes.pos} variant="contained"  color="primary">View</Button></Link>
                        <Link to={`/authors/${author.id}`} className={classes.a}><Button size="small" className={classes.pos} variant="contained"  color="secondary">DELETE</Button></Link>
                    </CardActions>
                    </Card>)))):(publication.map((publication =>
                    <Card className={classes.card} key={publication.id}>
                    <CardContent>
                        <Typography className={classes.title} variant="h5" component="h2" color={"textSecondary"}  gutterBottom>
                          Publication's Name :  { publication.title}
                        </Typography>
                        <Typography className={classes.email}  component="p"  color={"textSecondary"}>
                          Description :  { publication.body }
                        </Typography>
                        <Typography className={classes.email}  component="p"  color={"textSecondary"}>
                           Created By : { publication.created_by }
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Link to={`/authors/${publication.id}/publications`} className={classes.a}><Button size="small" className={classes.pos} variant="contained"  color="primary">EDIT</Button></Link>
                    </CardActions>
                </Card>)))}
                 <CardActions className={classes.fab}>
                     <Link to={/authors/}>
                         <Fab color="primary" aria-label="Add" className={classes.fab}>
                             <AddIcon/>
                         </Fab>
                     </Link>
                 </CardActions>
            </div>
        )
    }
}

const styles = theme => ({
    card: {
        maxWidth:255,
        height: 150,
        padding: 8,
        margin: 10,
        border: '1px solid',
    },
    email: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
       margin: theme.spacing.unit,
    },
    a: {
        textDecoration: 'none',
    },
    fab: {
      margin: theme.spacing.unit,
      float: 'right',
    }

});


export default withStyles(styles)(Cards);