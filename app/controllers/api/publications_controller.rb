class Api::PublicationsController < ApplicationController
  before_action :set_author
  before_action :set_author_publication, only: [:show, :update, :destroy]

  def index
    json_response(@author.publications)
  end

  def show
    @publications = Publication.order(date_and_time: :desc)
    json_response(@publications)
  end

  def create
    @author.publications.create!(publication_params)
    json_response(@author, :created)
  end

  def update
    @publication.update!(publication_params)
    json_response(@publication, 200)
  end

  def destroy
    @publication.destroy
  end

  def find_by_title
    @publication = Publication.find_by :title
    @paginate = @publication.paginate(page: params[:page], per_page: 20)
    json_response(@paginate, 200)
  end

  private

  def publication_params
    params.permit(:title, :body)
  end

  def set_author
    @author = Author.find(params[:author_id])
  end

  def set_author_publication
    @publication = @author.publications.find_by!(id: params[:id]) if @author
  end
end