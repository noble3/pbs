Rails.application.routes.draw do
  root to: 'pages#home'


  namespace :api, defaults: { format: :json } do
    resources :authors do
      resources :publications
    end
  end
end


