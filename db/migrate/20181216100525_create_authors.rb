class CreateAuthors < ActiveRecord::Migration[5.1]
  def change
    create_table :authors do |t|
      t.string :name
      t.string :email
      t.string :created_by
      t.belongs_to :publications
      t.timestamps
    end
  end
end
