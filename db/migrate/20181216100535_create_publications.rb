class CreatePublications < ActiveRecord::Migration[5.1]
  def change
    create_table :publications do |t|
      t.string :title
      t.string :body
      t.string :created_by
      t.references :author, foreign_key: true
      t.datetime :date_and_time
      t.timestamps
    end
  end
end
