require 'factory_bot_rails'

FactoryBot.define do
  factory :publications, class: 'Publication' do
    title { Faker::String.random(5)}
    created_by { Faker::Number.number(10) }
    body { Faker::String.random(5) }
    trait :authors do
      association(publications,factory: :create_author)
    end
  end
end