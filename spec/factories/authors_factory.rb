require 'factory_bot_rails'
require 'faker'


FactoryBot.define do
  factory :create_author, class: 'Author' do
    name { Faker::Name }
    email { Faker::Internet.email }
    created_by { Faker::Number.number(10) }
  end
end