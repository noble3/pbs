require 'rails_helper'

RSpec.describe 'Publications API' do
  # Initialize the test data
  let!(:author) { create(:create_author) }
  let(:author_id) { author.id }
  let!(:publications) { create_list(:publications, 20, author: author, title: 'Some title',body: 'Some text') }

  # Test suite for GET /api/authors/:author_id/publications
  describe 'GET /api/authors/:author_id/publications' do
    before { get "/api/authors/#{author_id}/publications" }

    context 'when author exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns all authors publications' do
        expect(json.size) == 1
      end
    end

    context 'when author does not exist' do
      let(:author_id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to include("Couldn't find Author")
      end
    end
  end

  # Test suite for GET /api/authors/:author_id/publications/:id
  describe 'GET /api/authors/:author_id/publications/:id' do
    before { get "/api/authors/#{author_id}/publications/#{iD}" }

    context 'when author publication exist' do

      let(:iD) { publications.first.id }

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns the publication' do
        expect(json['id']).to eq(iD)
      end
    end

    context 'when author publication does not exist' do
      let(:iD) { 2333 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to include("Couldn't find Publication with ")
      end
    end
  end

  # Test suite for PUT /authors/:author_id/publications
  describe 'POST /api/authors/:author_id/publications' do
    let(:valid_attributes) { { title: 'Moby Dick', body: 'Some content' } }

    context 'when request attributes are valid' do
      before { post "/api/authors/#{author_id}/publications", params: valid_attributes }

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when an invalid request' do
      before { post "/api/authors/#{author_id}/publications", params: {} }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a failure message' do
        expect(response.body).to include("Validation failed: Title can't be blank, Body can't be blank")
      end
    end
  end

  # Test suite for PUT /author/:author_id/publications/:id
  describe 'PUT /api/authors/:author_id/publications/:id' do
    let(:iD) { publications.first.id }
    let(:valid_attributes) { { title: 'Moby Dick' } }

    before do
      put "/api/authors/#{author_id}/publications/#{iD}", params: valid_attributes
    end

    context 'when publication exists' do
      it 'returns status code 204' do
        expect(response).to have_http_status(200)
      end

      it 'updates the publication' do
        expect(response.body).to include('Moby Dick')
      end
    end

    context 'when the publication does not exist' do
      let(:iD) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to include("Couldn't find Publication")
      end
    end
  end

  # Test suite for DELETE /api/authors/:id
  describe 'DELETE /api/authors/:id' do
    let(:iD) { publications.first.id }
    before { delete "/api/authors/#{author_id}/publications/#{iD}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end

  # Test suite for Find_By TITLE /api/authors/:id/publications/:title
  describe '/api/authors/:id/publications/:title' do
    let(:valid_attributes) { { title: 'Some Title' } }
    let(:iD) { publications.first.id }

    before { get "/api/authors/#{author_id}/publications/#{iD}",params: valid_attributes }

    it 'returns the publication title ' do
      expect(response.body).to include("Some title")
    end
  end
end