require 'rails_helper'
require 'factory_bot_rails'

RSpec.describe 'Authors API', type: :request do

#Intialize test data
  let!(:aut) { create_list(:create_author,10) }
  let(:aut_id) { aut.first.id }


  # Test suite for GET /authors
  describe 'GET /api/authors' do
    # make HTTP get request before each example
    before { get '/api/authors' }

    it 'returns authors' do
      # Note `json` is a custom helper to parse JSON responses
      expect(json).not_to be_empty
      expect(json.size).to eq(27)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  # Test suite for GET /authors/:id
  describe 'GET /api/authors/:id' do
    before { get "/api/authors/#{aut_id}" }

    context 'when the record exists' do
      it 'returns the author' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(aut_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:aut_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Author/)
      end
    end
  end

  # Test suite for POST /author
  describe 'POST /api/authors' do
    # valid payload
    let(:valid_attributes) { { id: 1,name: 'Learn Elm', email: 'jane@doe.com', created_by: '1' } }

    context 'when the request is valid' do
      before { post '/api/authors', params: valid_attributes }

      it 'creates a authors' do
        expect(json['name']) == 'John'
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post '/api/authors', params: { title: 'Foobar' } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
            .to include("Validation failed: Name can't be blank, Email can't be blank, Created by can't be blank")
      end
    end
  end

  # Test suite for PUT /author/:id
  describe 'PUT /api/authors/:id' do
    let(:valid_attributes) { { title: 'Shopping' } }

    context 'when the record exists' do
      before { put "/api/authors/#{aut_id}", params: valid_attributes }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  # Test suite for DELETE /author/:id
  describe 'DELETE /api/authors/:id' do
    before { delete "/api/authors/#{aut_id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end