require "rails_helper"

RSpec.describe Author, type: :model do

  context 'with 2 or more comments' do
    it 'orders them in reverse chronologically' do
      author = Author.create!(name: "Jane", email: "jane@doe.com", created_by: "jane")
      publication1 = author.publications.create!(title: 'Learn Elm)t',body: "Description")
      publication2 = author.publications.create!(title: 'second comment',body: "Description")
      expect(author.reload.publications) == [publication2, publication1]
    end
  end
end